### Supports and housings for Rotation Stage (ELL18) and Rotation Mount (ELL14)

ELL14 housing:
- Holes are sized so that M4 / M3 screws should cut their own threads as they are inserted.
- Slot in center for "Captive" M4 nut.

ELL18:
- Vertical and Horizontal plates and brackets.
- Bracket also has place for "Captive" M4 nut.

Housing to hold fanout Bus Distributor (ELLB) and PCB for Ethernet control.
